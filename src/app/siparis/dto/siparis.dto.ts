import { Time } from '@angular/common';

export class SiparisDto {

    productId: string;
    code: string;
    productName: string;
    categoryId: string;
    price: number;
    companyId: string;
    isDeleted: boolean;
    createBy: string;
    updateBy: string;
    // CreatedAt: Time;
    // UpdatedAt: Time;

}
